package de.byteterm.language;

import de.byteterm.jlogger.util.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class Language {

    private final String country;
    private final String path;

    private final Map<String, String> contentList;

    public Language(@NotNull String country, @NotNull String path) {
        this.country = country;
        this.path = path;
        this.contentList = new HashMap<>();
        this.build();
    }

    private void build() {
        Map<String, String> generated = FileUtils.readLang(path + "/" + country + ".lang");
        for(String key : generated.keySet()) {
            this.contentList.put(key, generated.get(key));
        }
    }

    public void reload() {
        this.build();
    }

    public String getMessage(@NotNull String key) {
        if(!(contentList.containsKey(key))) {
            return "Error code [ TMWN ]";
        }
        return this.getContentList().get(key);
    }

    public String getCountry() {
        return this.country;
    }

    public String getPath() {
        return this.path;
    }

    public Map<String, String> getContentList() {
        return this.contentList;
    }
}
