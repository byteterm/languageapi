package de.byteterm.language;

import de.byteterm.jlogger.Logger;
import de.byteterm.jlogger.util.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class LanguageAPI {

    private final Logger logger = Logger.getLogger();

    private static LanguageAPI instance;

    private final Map<String, Language> loadedLanguages;

    public LanguageAPI() {
        logger.info("Starting Language System...");
        this.loadedLanguages = new HashMap<>();
        instance = this;
    }

    public void loadAll(@NotNull String path) {
        String directory = FileUtils.getRunningJarParent(this.getClass()).getAbsolutePath() + "/" + path;
        File languageDirectory = new File(directory);

        if(languageDirectory.exists()) {
            for(File file : Objects.requireNonNull(languageDirectory.listFiles())) {
                if(file.getName().endsWith(".lang")) {
                    String name = file.getName().split("\\.")[0];
                    if(loadedLanguages.containsKey(name)) {
                        continue;
                    }
                    this.loadedLanguages.put(name, new Language(name, directory));
                    logger.info("Added language [ " + name + " ] successfully!");
                }
            }
        } else {
            logger.warn("Can't found path [ " + directory + " ] abort language loading...");
        }
    }

    public void load(@NotNull String name, @NotNull String path) {
        String directory = FileUtils.getRunningJarParent(this.getClass()).getAbsolutePath() + "/" + path;
        if(loadedLanguages.containsKey(name)) {
            logger.warn("This language [ " + name + " ] is already loaded!");
            return;
        }
        this.loadedLanguages.put(name, new Language(name, directory));
        logger.info("Added language [ " + name + " ] successfully!");
    }

    public void reload() {
        if(loadedLanguages.isEmpty()) {
            logger.warn("Nothing to reload!");
            return;
        }
        for(Language language : loadedLanguages.values()) {
            language.reload();
        }
    }

    public Map<String, Language> getLoadedLanguages() {
        return this.loadedLanguages;
    }

    public Logger getLogger() {
        return this.logger;
    }

    public static LanguageAPI getInstance() {
        return instance;
    }
}
